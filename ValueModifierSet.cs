using System.Collections.Generic;
/// <summary>
/// Value set.
/// 
/// </summary>  

public abstract class ValueModifierSet : ChangeableData {

	public static ValueModifierSet empty;
	protected ValueModifier[] combinedModifierArray; 

	/// <summary>
	/// The combined modifiers, one for each property.
	/// </summary>
	/// <param name="indexer">Indexer.</param>
	public ValueModifier this [int indexer] {
		get {
			return combinedModifierArray[indexer];
		}
	}

	public int Length {
		get {
			return combinedModifierArray.Length;
		}
	}

	protected virtual void UpdateCombinedModifierArray () {}
}

public class ValueModifier {
	public int indexer;
	public float addend;
	public float multiplier;

	public ValueModifier () {
		Reset ();
	}

	public void Reset () {
		addend = 0;
		multiplier = 1;
	}

	public float Apply (float f)
	{
		return f * multiplier + addend;
	}
}

public class ValueModifierSetFixed : ValueModifierSet {


	public ValueModifierSetFixed (int valueTypeCount, ValueModifier[] modArray) {
		combinedModifierArray = new ValueModifier[valueTypeCount];
		for (int i = 0; i < valueTypeCount; i++) {
			combinedModifierArray[i] = new ValueModifier();

			for (int h = 0; h < modArray.Length; h++) {
				if (modArray[h].indexer == i) {
					combinedModifierArray[i] = modArray[h];
//					if (combinedModifierArray[i] == null) {
//						combinedModifierArray[i] = modArray[h];
//					} else {
//						// duplicate modifier for single value. Throw exception.
//					}
				}
			}
		}
	}
}

public class ValueModifierSetArray : ValueModifierSet {
	ValueModifierSet[] setArray;

	public ValueModifierSetArray (int valueTypeCount, int modSetCount) {
		combinedModifierArray = new ValueModifier[valueTypeCount];
		setArray = new ValueModifierSet[modSetCount];
		UpdateCombinedModifierArray ();
	}

	public void SetValueModifierSet (int index, ValueModifierSet newSet) {
		if (setArray[index] == newSet) return; 

		if (setArray[index] != null) {
			setArray[index].Changed -= UpdateCombinedModifierArray;	
		}

		setArray[index] = newSet;	

		if (setArray[index] != null) {
			setArray[index].Changed += UpdateCombinedModifierArray;
		}

		UpdateCombinedModifierArray();
	}

	#region implemented abstract members of ValueModifierSet
	protected override void UpdateCombinedModifierArray ()
	{
		for (int h = 0; h < Length; h++) {
			combinedModifierArray[h].Reset();

			for (int i = 0; i < setArray.Length; i++) {
				if (setArray[i] == null) continue;

				combinedModifierArray[h].addend += setArray[i][h].addend;
				combinedModifierArray[h].multiplier *= setArray[i][h].multiplier;
			}
		}
	}
	#endregion
}

//public class ValueModifierSetList : ValueModifierSet {
//	List<ValueModifierSet> modifierList;
//
//	#region implemented abstract members of ValueModifierSet
//	protected override void UpdateCombinedModifierArray ()
//	{
//		for (int h = 0; h < Length; h++) {
//			combinedModifierArray[h].Reset();
//			
//			for (int i = 0; i < setArray.Length; i++) {
//				
//				combinedModifierArray[h].addend += setArray[i][h].addend;
//				combinedModifierArray[h].multiplier *= setArray[i][h];
//			}
//		}
//	}
//
//	#endregion
//}

