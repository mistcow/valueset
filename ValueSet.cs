using System.Collections.Generic;
/// <summary>
/// Value set.
/// 
/// </summary>  

public delegate void DataChangedHandler();

public class ChangeableData {
	public event DataChangedHandler Changed;

	public void OnChanged () {
		if (Changed != null) {
			Changed();
		}
	}
}

public abstract class ValueSet : ChangeableData {

	
	public abstract float this [int indexer] {
		get;
	}

	public abstract int Length {
		get;
	}

}

public class SimpleValueSet : ValueSet {
	float[] values;

	public SimpleValueSet (float[] newValues) {
		values = newValues;
	}

	public override float this [int indexer] {
		get {
			return values[indexer];
		}
	}
	public override int Length {
		get {
			return values.Length;	
		}
	}
}

public class ComplexValueSet : ValueSet {
	float[] effectedValues;
	ValueSet baseValueSet;
	ValueModifierSet valueModifierSet;

	public ComplexValueSet (ValueSet newBaseValueSet, ValueModifierSet newSet) {
		baseValueSet = newBaseValueSet;
		baseValueSet.Changed += UpdateValues;

		valueModifierSet = newSet;
		valueModifierSet.Changed += UpdateValues;

		effectedValues = new float[baseValueSet.Length];

		UpdateValues();
	}

	/// Get the effected value, i.e, baseValue modified by affections.	
	public override float this [int indexer] {
		get {
			return effectedValues[indexer];
		}
	}
	
	public override int Length {
		get {
			return effectedValues.Length;	
		}
	}
	
	public void SetBaseValueSet (ValueSet newBaseValueSet) {
		if (baseValueSet != newBaseValueSet) {

			if (baseValueSet != null)
				baseValueSet.Changed -= UpdateValues;

			baseValueSet = newBaseValueSet;

			if (baseValueSet != null)
				baseValueSet.Changed += UpdateValues;

			UpdateValues ();
		}
	}
	
	public void SetValueModifier (ValueModifierSet newSet) {
		if (valueModifierSet != newSet) {

			if (valueModifierSet != null)
				valueModifierSet.Changed -= UpdateValues;

			valueModifierSet = newSet;

			if (valueModifierSet != null)
				valueModifierSet.Changed += UpdateValues;
			
			UpdateValues ();
		}
	}
	
	void UpdateValues ()
	{
		for (int i = 0; i < baseValueSet.Length; i++) {
			effectedValues[i] = valueModifierSet[i].Apply(baseValueSet[i]);
		}
	}
}




